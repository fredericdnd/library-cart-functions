export default class CartBook {
  bookId: string;
  quantity: number;

  constructor(bookId: string, quantity: number) {
    this.bookId = bookId;
    this.quantity = quantity;
  }

  public static map(json: any): CartBook {
    return new this(json.bookId, json.quantity);
  }
}
