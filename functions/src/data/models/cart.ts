import CartBook from "./cart-book";

export default class Cart {
  id: string;
  cartBooks: CartBook[];

  constructor(id: string, cartBooks: CartBook[]) {
    this.id = id;
    this.cartBooks = cartBooks;
  }

  public static map(json: any): Cart {
    return new this(json.id, json.cartBooks);
  }
}
