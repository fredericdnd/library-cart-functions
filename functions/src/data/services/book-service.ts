import { firestore } from "firebase-admin";
import Book from "../models/book";

export default class BookService {
  private static instance: BookService;

  collection = firestore().collection("books");

  static get shared(): BookService {
    if (!this.instance) {
      this.instance = new this();
    }

    return this.instance;
  }

  public getBook(id: string) {
    return this.collection
      .doc(id)
      .get()
      .then((snapshot) => Book.map(snapshot.data()));
  }

  public getBooks(ids: [string]) {
    const promises: Promise<Book>[] = [];

    ids.forEach((id) => {
      promises.push(this.getBook(id));
    });

    return Promise.all(promises);
  }
}
