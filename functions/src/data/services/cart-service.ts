import { firestore } from "firebase-admin";
import Book from "../models/book";
import Cart from "../models/cart";
import CartBook from "../models/cart-book";
import BookService from "./book-service";
import axios from "axios";
import * as functions from "firebase-functions";
import LibraryResponse from "../models/library-response";

class CartService {
  private static instance: CartService;

  collection = firestore().collection("carts");
  libraryBaseUrl: string = functions.config().library.base_url;

  static get shared(): CartService {
    if (!this.instance) {
      this.instance = new this();
    }

    return this.instance;
  }

  addBookToCart(
    cartId: string | null,
    bookId: string,
    quantity: number
  ): Promise<LibraryResponse> {
    if (cartId) {
      return this.collection
        .doc(cartId)
        .get()
        .then((snapshot) => {
          if (!snapshot.exists) {
            return this.createCart(bookId, quantity);
          }

          const cart = Cart.map(snapshot.data());
          const foundBook = cart.cartBooks.find(
            (cartBook) => cartBook.bookId === bookId
          );

          if (foundBook) {
            foundBook.quantity += quantity;
          } else {
            const cartBook = new CartBook(bookId, quantity);
            cart.cartBooks.push(cartBook);
          }

          return this.collection
            .doc(cartId)
            .update(JSON.parse(JSON.stringify(cart)))
            .then(() => {
              return Promise.resolve(
                new LibraryResponse("Books added to cart", cart)
              );
            });
        });
    }

    return this.createCart(bookId, quantity);
  }

  createCart(bookId: string, quantity: number) {
    const newCartId = this.collection.doc().id;
    const cart = new Cart(newCartId, [new CartBook(bookId, quantity)]);

    return this.collection
      .doc(newCartId)
      .set(JSON.parse(JSON.stringify(cart)))
      .then(() => {
        return Promise.resolve(new LibraryResponse("Cart initialized", cart));
      });
  }

  getCartBooks(cartId: string) {
    const bookPromises: Promise<Book>[] = [];

    return this.collection
      .doc(cartId)
      .get()
      .then((document) => {
        document.data()?.bookIds.forEach((bookId: string) => {
          bookPromises.push(BookService.shared.getBook(bookId));
        });

        return Promise.all(bookPromises);
      });
  }

  getCartPrice(cartId: string) {
    return this.getCartBooks(cartId).then((books) =>
      books.reduce(function (prev, book) {
        return prev + book.price;
      }, 0)
    );
  }

  validateCart(cartId: string) {
    return this.collection
      .doc(cartId)
      .get()
      .then((snapshot) => {
        const cart = Cart.map(snapshot.data());

        const promises = cart.cartBooks.map((cartBook) => {
          const url = `${this.libraryBaseUrl}/updateBookStock`;

          const data = JSON.stringify({
            bookId: cartBook.bookId,
            stock: -cartBook.quantity, // - because we need to remove the quantity to the book stock
          });

          return axios({
            method: "POST",
            headers: { "content-type": "application/json" },
            data: data,
            url,
          });
        });

        return Promise.all(promises)
          .then(() => this.collection.doc(cartId).delete())
          .then(() => {
            return Promise.resolve(
              new LibraryResponse("Cart validated and cleared", cart)
            );
          });
      });
  }
}

export default CartService;
