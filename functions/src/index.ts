import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import CartService from "./data/services/cart-service";

admin.initializeApp();

const cartService = CartService.shared;

export const getCartBooks = functions.https.onRequest((req: any, res: any) => {
  const cartId = req.query.cartId;

  if (req.method !== "GET") {
    return res.status(403).send("Unauthorized");
  } else if (cartId === undefined || cartId.length === 0) {
    return res.status(400).send("Invalid cartId");
  }

  return cartService
    .getCartBooks(cartId)
    .then((books) => res.status(200).send(books))
    .catch((error) => res.status(400).send(error));
});

export const getCartPrice = functions.https.onRequest((req: any, res: any) => {
  const cartId = req.query.cartId;

  if (req.method !== "GET") {
    return res.status(403).send("Unauthorized");
  } else if (cartId === undefined || cartId.length === 0) {
    return res.status(400).send("Invalid cartId");
  }

  return cartService
    .getCartPrice(cartId)
    .then((price) => res.status(200).send(price))
    .catch((error) => res.status(400).send(error));
});

export const addBookToCart = functions.https.onRequest((req: any, res: any) => {
  console.log(functions.config());

  const cartId = req.body.cartId;
  const bookId = req.body.bookId;
  const quantity = req.body.quantity;

  if (req.method !== "POST") {
    return res.status(403).send("Unauthorized");
  } else if (bookId === undefined) {
    return res.status(400).send("Invalid bookId");
  } else if (quantity === undefined) {
    return res.status(400).send("Invalid quantity");
  }

  return cartService
    .addBookToCart(cartId, bookId, quantity)
    .then((response) => {
      res.status(201).send(response);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
});

export const validateCart = functions.https.onRequest((req: any, res: any) => {
  const cartId = req.body.cartId;

  if (req.method !== "POST") {
    return res.status(403).send("Unauthorized");
  } else if (cartId === undefined) {
    return res.status(400).send("Invalid cartId");
  }

  return cartService
    .validateCart(cartId)
    .then((response) => {
      res.status(201).send(response);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
});
